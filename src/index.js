import React from 'react';
import {useState} from "react";
import ReactDOM from 'react-dom';

import PokeCard from './PokeCard'
import useStyles from "./style"
import PokeDialog from './PokeDialog'
import usePoke from "./usePoke";

import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import RadioGroup from '@material-ui/core/RadioGroup';
import '@fontsource/roboto';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import { createTheme, ThemeProvider } from '@material-ui/core/styles';
import { InputBase } from '@material-ui/core';





import {useEffect} from "react";









const Pokedex = () => {



  const innerTheme = createTheme({
    typography: {
      fontFamily: 'Russo One',
    }
});


  const { 
    handleClose,
    open,
    pokeDia,
    handleMoreInfo
  } = usePoke();

  const classes = useStyles();


  const [search, setSearch] = useState("")


  const [pokeList , setPokeList] = useState([])



  const [gen, setGen] = useState(151)


  let loading = false
  if (pokeList.length > gen) {  
    loading = true}



  const changeGen = (e) => {
    setGen(e.target.value)

  }


  const fetchPokemon = () => {
   const pokeUrl = id => `https://pokeapi.co/api/v2/pokemon/${id}`
     

    for (let i = 1; i <= gen; i++) {
      fetch(pokeUrl(i)).then(res => res.json()).then(r => setPokeList(old => [...old, r]))
    }
  }


  const load = () => {
    if (pokeList.length > gen) {  
    loading = true
    return (
      <div className={classes.container}>
      {pokeList.filter((val) => {
        if(search == "") {
          return val
        } else if (val.name.toLowerCase().includes(search.toLowerCase()) ){
          return val
        }
      }).sort((a, b) => (a.id > b.id) ? 1 : -1).map((card, index) =>
        <div key={index}>
        <PokeCard card = {card}   handleMoreInfo = {() => handleMoreInfo(card)}/>
        </div> 
       )}
    </div>
    )
  }else {
    return (
      <div className={classes.load}>
      <CircularProgress/>
      </div>
    )
  }
}

  useEffect(() => {
    setPokeList([
      {
        name:"tiratone",
        types: [{type:{name:"tiramissu"}},{type:{name:"panetone"}}],
        abilities: [{ability:{name:"feed"}}],
        sprites: {front_default: "https://i.postimg.cc/VsVY1Cyb/pixil-frame-0-4.png"},
        id: 999,
      },
    ])

    loading = false    
    fetchPokemon()

        
  }, [gen])






   return ( 
    <div>
          <div className={classes.head}>
        
        <Typography className={classes.title} variant="h2" gutterBottom>
          PokeDex
        </Typography>
        <InputBase className={classes.search} placeholder="Search" variant="outlined" onChange={e => setSearch(e.target.value)} />
        <FormControl component="fieldset">
          <RadioGroup row aria-label="position" name="position" defaultValue="151" >
            <FormControlLabel
              value="151"
              control={<Radio color="primary" />}
              label="1º"
              labelPlacement="top"
              onChange={changeGen}
              disabled = {!loading}
           />
            <FormControlLabel
              value="251"
              control={<Radio color="primary" />}
              label="2º"
              labelPlacement="top"
              onChange={changeGen}
              disabled = {!loading}
            />
            <FormControlLabel
              value="386"
              control={<Radio color="primary" />}
              label="3º"
              labelPlacement="top"
              onChange={changeGen}
              disabled = {!loading}
            />
          </RadioGroup>
        </FormControl>
      </div>
      <div>
        {
          load()
        }

      </div>
      <PokeDialog pokeDia={pokeDia}  open={open} handleClose={handleClose} />
    </div>  
  )
  
}



ReactDOM.render(
  <Pokedex/>,
  document.getElementById('root')
);