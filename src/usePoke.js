import {useState} from "react";


const usePoke = () => {

  const [pokeDia, setPokeDia] = useState({
    name:"error",
    types: [],
    abilities: []
  })


  const [open, setOpen] = useState(false)


  const handleClose = () => {
    setOpen(false);
  }

  const handleMoreInfo = (p) => {
    setOpen(true);
    setPokeDia(p);
  }
  

  const [search, setSearch] = useState("")

  const [pokeList , setPokeList] = useState([])

  const [gen, setGen] = useState(151)

  let loading = false

  const changeGen = (e) => {
    setGen(e.target.value)

  }



    return (
     {
      pokeDia,
      setPokeDia,
      open,
      setOpen,
      handleClose,
      handleMoreInfo,
      search,
      setSearch,
      pokeList,
      setPokeList,
      gen,
      setGen,
      loading,
      changeGen
     }

    )

}

export default usePoke;