import React from 'react';


import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';



const PokeDialog = ({pokeDia, open, handleClose}) => {

  return (
    <Dialog
      open={open}
    >
      <DialogTitle>{pokeDia.name}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Moves:
          <div>{pokeDia.abilities.map(abilityInfo => abilityInfo.ability.name).join(' | ')}</div>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
        variant="contained"
        onClick={handleClose}
        >
          Close
        </Button>
      </DialogActions>
    </Dialog>
    )
}  

export default PokeDialog
