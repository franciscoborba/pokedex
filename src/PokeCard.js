import React from 'react';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Chip from '@material-ui/core/Chip';
import Typography from '@material-ui/core/Typography';

import { createTheme, ThemeProvider } from '@material-ui/core/styles';

import useStyles from "./style"


function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}


const innerTheme = createTheme({
    typography: {
      fontFamily: 'Russo One',
    }
});




const PokeCard = ({handleMoreInfo, card}) => {

  const classes = useStyles();


    return(

        <Card className={[classes.root, classes[card.types.map(typeInfo => typeInfo.type.name)[0]]]}  onClick={handleMoreInfo}>
          <CardMedia
            className={classes.media}
            component="img"
            image={card.sprites.front_default}
          />
          <CardContent className={classes.cardContend}>
          <ThemeProvider theme={innerTheme}>
            <Typography variant="h6" gutterBottom>
            #{card.id} - {capitalizeFirstLetter(card.name)}
            </Typography>
          </ThemeProvider>
            <Chip className={classes[card.types.map(typeInfo => typeInfo.type.name)]}   label={card.types.map(typeInfo => typeInfo.type.name).join(' | ')} />
          </CardContent>
        </Card>
    )
  }

  export default PokeCard;