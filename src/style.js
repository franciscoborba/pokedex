import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
      maxWidth: 245,
      margin: "10px",
      borderColor: "#FFFFFF",
      borderWidth: "3px",
      borderStyle: "solid",
      borderRadius: "20px",
      "&:hover": {
        borderWidth: "5px",
        cursor: "pointer",
        margin: "8px"
   },
    },
    head : {
      display: "flex",
      justifyContent: "space-between",
      height: "100px",
      backgroundColor: "#ffffff",
      borderBottom: "solid",
      borderWidth: "30px",
      borderColor: "#BB5F62",
      alignItems: "center",
      position: "sticky",
      top: "0",
      paddingLeft: "20px",
      
    },
    media: {
      height: 190,
    },
    container: {
      display: "flex",
      flexWrap: "wrap",
      flexDirection: "row",
      textAlign: "center",
      justifyContent: "center",
      marginTop: "1%",
    },
    cardContend: {
      padding: "5px"
    },
    fire : {
      borderColor: "#EE8130",
      
    },
    steel : {
      borderColor: "#B7B7CE",
    },
    grass : {
      borderColor: "#7AC74C",
    },
    electric : {
      borderColor: "#F7D02C",
    },
    water : {
      borderColor: "#6390F0",
    },
    ground : {
      borderColor: "#E2BF65",
    },
    rock : {
      borderColor: "#B6A136",
    },
    fairy : {
      borderColor: "#D685AD",
    },
    poison : {
      borderColor: "#A33EA1",
    },
    bug : {
      borderColor: "#A6B91A",
    },
    dragon : {
      borderColor: "#97b3e6",
    },
    psychic : {
      borderColor: "#eaeda1",
    },
    fighting : {
      borderColor: "#C22E28",
    },
    normal : {
      borderColor: "#A8A77A",
    },
    ghost :{
      borderColor: "#735797",
    },
    ice :{
      borderColor: "#96D9D6",
    },
    flying :{
      borderColor: "#A98FF3",
    },
    dark: {
      borderColor: "#705848",
    },
    tiramissu: {
      borderColor: "#96D9D6"
    },
    search: {
      width: "500px",
      border: "2px solid black",
      borderRadius: "20px",
      height: "50px",
      padding: "15px"
    },
    title: {
     color: "black",
     textAlign: "center",
     fontFamily: 'Train One',
    },
    load: {
      display: "flex",
      justifyContent: "center",
      padding: "80px"
    }
  });

  export default useStyles